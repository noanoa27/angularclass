import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  email:string;
  password:string;
  name:string;
  code='';
  message='';


  signup(){ //פרומיס חד פעמי מהסיבה הזו צריך דאן
    this.authService.signup(this.email,this.password)//כל זה מחזיר promise
    .then( value =>{
      this.authService.updateProfile(value.user,this.name);
      this.authService.addUser(value.user,this.name);
    }).then(value =>{
      this.router.navigate(['/'])//אחרי שעושים הרשמה זה עובר לדף הראשי טודוס
    }).catch(err =>{
      this.code=err.code;
      this.message=err.message;
      console.log(err);
    }) 
  }
  constructor(private authService:AuthService,private router:Router) { } //רוצים לייצר את האובייקט של authservice

  ngOnInit() {
  }

}
