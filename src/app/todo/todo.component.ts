import { Component, OnInit, Input ,Output,EventEmitter} from '@angular/core';
import { TodosService } from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})



export class TodoComponent implements OnInit {
  
  @Input() data:any; //אינפוט בשם דטה 
@Output() myButtonClicked= new EventEmitter<any>();
  text;
  id;
  key;
  tempText;

  showTheButton=false;
  showEditField=false;

  delete(){
    this.TodosService.delete(this.key)
  }
  showEdit(){ //אדיט של טודו
    this.showEditField=true;
    this.tempText=this.text;
  
  }
  save(){
    this.TodosService.update(this.key,this.text)
    this.showEditField=false;
  }
  cancel(){
    this.showEditField=false;
    this.text=this.tempText;
  }
showButton(){
  this.showTheButton=true;
}
hideButton(){
  this.showTheButton=false;
}

send(){
  console.log('event caught');
  this.myButtonClicked.emit(this.text);
}

  constructor(private TodosService:TodosService) { }

  ngOnInit() {
    this.text = this.data.text;
    this.id = this.data.id;
    this.key=this.data.$key; //מעדכנת אתהפרמטר קי
  }

}
