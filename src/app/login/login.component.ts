import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  error='';

  login(){
    this.authService.login(this.email,this.password)//כל זה מחזיר promise
  .then(user =>{
      this.router.navigate(['/'])//אחרי שעושים הרשמה זה עובר לדף הראשי טודוס
    }).catch(err =>{
      this.error=err;
      console.log(err);
    }) 
  }
  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

}
