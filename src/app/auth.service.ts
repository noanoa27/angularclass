import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {Observable} from 'rxjs'; //לטעון את המחלקה אובסרוובל מתוך המחלקה rxjs
import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //פונקציה שמוסיפה לפיירבייס
  signup(email:string,password:string){
  return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email,password);
  }

  login(email:string,password:string){
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email,password)
  }


  updateProfile(user,name:string){
    user.updateProfile({displayName:name,photoURL:''});
  }

  logout(){
    return this.fireBaseAuth.auth.signOut();
  }

  addUser(user,name:string){
    let uid = user.uid;
    let ref = this.db.database.ref('/'); //האנד פוינט הראשי של דטה בייס
    ref.child('users').child(uid).push({'name':name}); //יוצרת אנד פוינט חדש
  }

  user:Observable<firebase.User>; // היוזר מסוג אובסרבבל
//האובסרבבל מסוג פיירבייס יוסר
  constructor(private fireBaseAuth:AngularFireAuth,
              private db:AngularFireDatabase) { 
  this.user=fireBaseAuth.authState;
  }
}
